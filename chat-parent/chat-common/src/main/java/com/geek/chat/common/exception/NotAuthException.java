package com.geek.chat.common.exception;

/**
 * 认证异常类
 */
public class NotAuthException extends ChatException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6388484514638425714L;

	public NotAuthException() {
        super("未登录认证");
    }
}
