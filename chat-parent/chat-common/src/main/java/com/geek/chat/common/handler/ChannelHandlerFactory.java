package com.geek.chat.common.handler;

import io.netty.channel.ChannelHandler;

public interface ChannelHandlerFactory {
	ChannelHandler build();
}
