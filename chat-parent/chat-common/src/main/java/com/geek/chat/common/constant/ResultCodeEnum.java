package com.geek.chat.common.constant;

/**
 * 返回码枚举类
 */
public enum ResultCodeEnum
{
    /** 成功 */
    SUCCESS(0, "Success"),
    AUTH_FAILED(-1,"授权失败"),
    NO_TOKEN(-2,"没有授权信息"),
    UNKNOW_DESTINATION(-3,"unknow destination"),
    ;

    private Integer code;
    private String desc;

    ResultCodeEnum(Integer code, String desc)
    {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode()
    {
        return code;
    }

    public String getDesc()
    {
        return desc;
    }

    public static void main(String[] args)
    {
        System.out.println(ResultCodeEnum.SUCCESS.getCode() + " - "
                + ResultCodeEnum.SUCCESS.getDesc());
        System.out.println(ResultCodeEnum.SUCCESS.getCode());
    }

}
