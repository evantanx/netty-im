package com.geek.chat.common;

import java.util.ArrayList;
import java.util.List;

import com.geek.chat.common.handler.ChannelHandlerFactory;
/**
 * 通用配置信息接口服务
 * @author lenovo
 *
 */
public interface NettyConfig {
	/**handler消息处理hadnler列表*/
	List<ChannelHandlerFactory> handlerList = new ArrayList<>();

	/**
	 * 抽象方法，用于子类中设置各自的消息处理Handler
	 * @param handlerList
	 */
	void setHandlerList(final List<ChannelHandlerFactory> handlerList);

	/**
	 * 获取绑定端口  tcp > port 端口号
	 * @return
	 */
	int getPort();
	
	String getServerIp();
}
