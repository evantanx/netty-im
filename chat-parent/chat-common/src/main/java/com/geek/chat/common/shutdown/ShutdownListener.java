package com.geek.chat.common.shutdown;
/**
 * 监听服务关闭
 * @author lenovo
 *
 */
public interface ShutdownListener {

	void shutdown();
}
