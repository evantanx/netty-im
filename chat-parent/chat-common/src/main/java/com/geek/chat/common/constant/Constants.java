package com.geek.chat.common.constant;

/**
 * Created by lenovo
 */
public class Constants {

	//登陆授权请求
    public static final int OP_AUTH = 1;
    //登陆授权应答
    public static final int OP_AUTH_REPLY = 2;

    //心跳请求
    public static final int OP_HEARTBEAT = 3;
    //心跳应答
    public static final int OP_HEARTBEAT_REPLY = 4;

    //IM消息请求
    public static final int OP_MESSAGE = 5;
    //IM消息应答
    public static final int OP_MESSAGE_REPLY = 6;

}
