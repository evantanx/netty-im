package com.geek.chat.common.shutdown;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 实现优雅关闭
 * @author lenovo
 */
public class Shutdown {
	private static final Logger logger = LoggerFactory.getLogger(Shutdown.class);

	private static Shutdown shutdown;
	
	/**实现可扩展，可添加多个listener*/
	private List<ShutdownListener> listeners = new ArrayList<ShutdownListener>();

	public synchronized static Shutdown sharedInstance() {
		if (shutdown == null) {
			shutdown = new Shutdown();
			shutdown.addShutdownHook();
		}
		return shutdown;
	}

	/**
	 * 钩子函数，实现优雅关闭，倾力资源
	 */
	private void addShutdownHook() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				for (ShutdownListener listener : listeners) {
					listener.shutdown();
				}

				logger.info("APP_SHUTDOWN_SUCCESSFULLY!");
			}
		});
	}

	public void addListener(ShutdownListener listener) {
		listeners.add(listener);
	}
}
