package com.geek.chat.common.util;

public class MsgUtil {
	/**
	 * 生成消息流水号,待优化，因为每一台汇总端设备的时间都是不好一样的。需要保证唯一性，避免消息ID重复
	 * @return
	 */
	public static long genSeqId(){
		return System.currentTimeMillis();
	}
}
