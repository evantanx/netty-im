package com.geek.chat.common;
/**
 * 服务生命周期的几个接口方法
 * @author lenovo
 *
 */
public interface LifeCycle {
	/**启动服务器*/
	void start();
	
	/**停止服务器*/
	void stop();
	
	/**销毁资源，释放*/
	void destroy();
}
