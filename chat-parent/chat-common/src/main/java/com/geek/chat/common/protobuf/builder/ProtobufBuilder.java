package com.geek.chat.common.protobuf.builder;

import org.apache.commons.lang.StringUtils;

import com.geek.chat.common.constant.Constants;
import com.geek.chat.common.constant.ResultCodeEnum;
import com.geek.chat.common.protobuf.ProtoMsg.Message;
import com.geek.chat.common.protobuf.ProtoMsg.Result;

public class ProtobufBuilder {
	/**
	 * 构建auth授权应答消息protobuf
	 * @param en
	 * @param seqId
	 * @return
	 */
	public static Message buildAuthRsp(ResultCodeEnum en,long seqId,String chatId){
		Message.Builder mb = Message.newBuilder()
				.setHeader(Constants.OP_AUTH_REPLY)  //设置消息类型
		        .setSeqId(seqId);                    //设置应答流水，与请求对应
		Result.Builder rb = Result.newBuilder()
				.setCode(en.getCode())
				.setInfo(en.getDesc())
				.setExpose(1);
		if(StringUtils.isNotEmpty(chatId)){
			//返回当前用户的chatId,后面聊天时候的会话ID
			rb.setChatId(chatId);
		}
		mb.setResult(rb.build());
		return mb.build();
	}
	
	/**
	 * 构建心跳请求消息
	 * @return
	 */
	public static Message buildHeartReq(long seqId){
		Message.Builder mb = Message.newBuilder()
				.setHeader(Constants.OP_HEARTBEAT)  //设置消息类型
		        .setSeqId(seqId);                   //设置应答流水，与请求对应
		return mb.build();
	}
	
	/**
	 * 构建心跳应答消息
	 * @return
	 */
	public static Message buildHeartRsp(long seqId){
		Message.Builder mb = Message.newBuilder()
				.setHeader(Constants.OP_HEARTBEAT_REPLY)   //设置消息类型
		        .setSeqId(seqId);                          //设置应答流水，与请求对应
		return mb.build();
	}
	
	public static Message buildChatResult(long seqId,ResultCodeEnum en){
		Message.Builder mb = Message.newBuilder()
				.setHeader(Constants.OP_MESSAGE_REPLY)  //设置消息类型
		        .setSeqId(seqId);                    //设置应答流水，与请求对应
		Result.Builder rb = Result.newBuilder()
				.setCode(en.getCode())
				.setInfo(en.getDesc())
				.setExpose(1);
		mb.setResult(rb.build());
		return mb.build();
	}
}
