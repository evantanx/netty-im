package com.geek.chat.common.exception;

/**
 * 聊天异常类
 */
public abstract class ChatException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -4977626538108881814L;

	public ChatException(String message) {
        super(message);
    }

    public ChatException(String message, Throwable cause) {
        super(message, cause);
    }

}
