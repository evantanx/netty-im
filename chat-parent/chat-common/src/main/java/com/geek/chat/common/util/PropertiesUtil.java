package com.geek.chat.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;

/**
 * Module ID: </br>
 * Comments：本类为Properties文件管理类</br>
 * Create Date: 2011-04-07
 * @author B_JIANQIANG
 */
public class PropertiesUtil {

	private static Logger logger = LoggerFactory.getLogger(PropertiesUtil.class);

	
	private static Properties property = null;

	private static String propertyFile = "/util.properties";
	
	private static String fileName = "util.properties";

	static {
		if (property == null) {
			loadFile(true);
		}
	}

	/**
	 * load property file(default property file is "/util.properties")
	 * 
	 * @param flag
	 * 
	 */
	public static void loadFile(boolean flag) {
		
		logger.debug("load property file is ---" + propertyFile);
		InputStream inStream = null;
		property = new Properties();
		try {
//			if (flag) {
//				inStream = PropertiesUtil.class.getResourceAsStream(propertyFile);
//			} else {
//				inStream = new FileInputStream(getOuterPropertiesFile());
//			}
			try {
				inStream = new FileInputStream(getOuterPropertiesFile());
			} catch (FileNotFoundException e) {
				logger.error("加载properties问价失败:{}",e.getMessage());
				System.out.println("load properties file: " + propertyFile);
				inStream = PropertiesUtil.class.getResourceAsStream(propertyFile);
			}
			property.load(inStream);
		} catch (Exception e) {
			if (flag) {
				loadFile(false);
			} else {
				e.printStackTrace();
			}
		} finally {
			try {
				if (inStream != null) {
					inStream.close();
				}
			} catch (IOException e) {
			}
		}
	}
	
	public static File getOuterPropertiesFile(){
		File file = new File("");
		File propertiesFile = new File(file.getAbsolutePath() + "/" + fileName);
		return propertiesFile;
	}

	/**
	 * get value from property file
	 * 
	 * @param key
	 * @return String
	 */
	public static String getProperty(String key) {
		return Utils.encodeStr(Utils.trimStr(property.getProperty(key)),
				System.getProperty("file.encoding"));
	}
	
	/**
	 * get int value from property file
	 * 
	 * @param key
	 * @return String
	 */
	public static Integer getInteger(String key,Integer defVal) {
		String s = property.getProperty(key);
		if(Utils.isInt(s)){
			return Integer.parseInt(s);
		} else {
			return defVal;
		}
	}
	
	/**
	 * get int value from property file
	 * 
	 * @param key
	 * @return String
	 */
	public static boolean getBoolean(String key,boolean defVal) {
		String s = property.getProperty(key);
		if(Utils.isBoolean(s)){
			return Boolean.valueOf(s);
		} else {
			return defVal;
		}
	}

	/**
	 * set property to sort to property file
	 * 
	 * @param key
	 * @param value
	 */
	public static void setProperty(String key, String value) {
		FileOutputStream fos = null;
		property.setProperty(key, value);
		try {
			//通过线程拿到当前的路径
			String path = Thread.currentThread().getContextClassLoader().getResource("/").getPath();
			//进行编码转换
			String decodePath = URLDecoder.decode(path,"utf-8");
			//System.out.println("----------- : "+decodePath);
			fos = new FileOutputStream(decodePath+fileName);
			property.store(fos, null);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @return Returns the property.
	 */
	public static Properties getProperty() {
		return property;
	}

	/**
	 * @param property The property to set.
	 */
	public static void setProperty(Properties property) {
		PropertiesUtil.property = property;
	}

	/**
	 * @return Returns the propertyFile.
	 */
	public static String getPropertyFile() {
		return propertyFile;
	}

	/**
	 * @param propertyFile The propertyFile to set.
	 */
	public static void setPropertyFile(String propertyFile) {
		if (!PropertiesUtil.propertyFile.equals(propertyFile)) {
			PropertiesUtil.propertyFile = propertyFile;
			PropertiesUtil.loadFile(true);
		}
	}
}
