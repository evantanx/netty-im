package com.geek.chat.common.constant;
/**
 * 客户端平台
 * @author lenovo
 *
 */
public interface Platform {
	/**android端*/
	public static final int ANDROID = 1;
	/**IOS端*/
	public static final int IOS = 2;
	/**WEB端*/
	public static final int WEB = 3;
	/**全部*/
	public static final int BOTH = 4;
}
