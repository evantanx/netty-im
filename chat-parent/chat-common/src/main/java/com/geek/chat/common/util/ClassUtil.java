package com.geek.chat.common.util;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ClassUtil {
	/**
	 * 到指定包下获取类
	 * @param cls
	 * @param packageName
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static List<Class<?>> getClasses(Class<?> cls,String packageName) throws IOException,
			ClassNotFoundException {
		List<Class<?>> classes = new ArrayList<Class<?>>();
		String path = packageName.replace('.', '/');
		ClassLoader classloader = Thread.currentThread()
				.getContextClassLoader();
		Enumeration<URL> urls = classloader.getResources(path);
		while (urls.hasMoreElements()) {
			URL url = urls.nextElement();
			String protool = url.getProtocol();
			if ("file".equals(protool)) {
				classes.addAll( getClasses(new File(URLDecoder.decode(url.getFile(),"UTF-8")), packageName));
			} else if ("jar".equals(protool)) {
				classes.addAll(doScanPackageClassesByJar(packageName,url));
			}
		}
		return classes;
	}
	
	/**
	 * 到指定包下获取jar包中的类
	 * @param basePackage
	 * @param url
	 * @return
	 */
	private static  List<Class<?>> doScanPackageClassesByJar(String basePackage, URL url) {
		List<Class<?>> classes = new ArrayList<Class<?>>();
		String packageName = basePackage;
		String package2Path = packageName.replace('.', '/');
		JarFile jar;
		try {
			jar = ((JarURLConnection) url.openConnection()).getJarFile();
			Enumeration<JarEntry> entries = jar.entries();
			while (entries.hasMoreElements()) {
				JarEntry entry = entries.nextElement();
				String name = entry.getName();
				if (!name.startsWith(package2Path) || entry.isDirectory()) {
					continue;
				}
				// 判断是否过滤 inner class
				if (name.indexOf('$') != -1) {
					continue;
				}
				// 判定是否符合过滤条件
				String className = name.replace('/', '.');
				className = className.substring(0, className.length() - 6);
				try {
					classes.add(Thread.currentThread().getContextClassLoader()
							.loadClass(className));
				} catch (ClassNotFoundException e) {
				}
			}
		} catch (IOException e) {
		}
		return classes;
	}
	
	/**
	 * 到指定包下获取classes中的类
	 * @param dir
	 * @param pk
	 * @return
	 * @throws ClassNotFoundException
	 */
	private static List<Class<?>> getClasses(File dir, String pk)
			throws ClassNotFoundException {
		List<Class<?>> classes = new ArrayList<Class<?>>();
		if (!dir.exists()) {
			return classes;
		}
		for (File f : dir.listFiles()) {
			if (f.isDirectory()) {
				classes.addAll(getClasses(f, pk + "." + f.getName()));
			}
			String name = f.getName();
			if (name.endsWith(".class")) {
				classes.add(Class.forName(pk + "."
						+ name.substring(0, name.length() - 6)));
			}
		}
		return classes;
	}
	
	public static String getMsgType(int megId){
		String hex = Integer.toHexString(megId);
		if(hex.length() == 1){
			hex = "0x000"+hex;
		} else if(hex.length() == 2){
			hex = "0x00"+hex;
		} else if(hex.length() == 3){
			hex = "0x0"+hex;
		} else{
			hex = "0x"+hex;
		}
		return hex;
	}
}
