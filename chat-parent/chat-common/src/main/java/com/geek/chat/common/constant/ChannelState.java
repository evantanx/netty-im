package com.geek.chat.common.constant;

public enum ChannelState {
	/**运行中*/
	RUNNING,
	/**已关闭*/
	CLOSED,
	/**连接中*/
	CONNECTING,
	/**关闭中*/
	CLOSING
}
