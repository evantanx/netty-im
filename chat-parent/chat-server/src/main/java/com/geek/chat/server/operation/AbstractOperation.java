package com.geek.chat.server.operation;

import com.geek.chat.common.exception.NotAuthException;
import com.geek.chat.common.server.config.AttributteKeys;

import io.netty.channel.Channel;

public abstract class AbstractOperation implements Operation {
	protected String getKey(Channel ch) {
		return ch.attr(AttributteKeys.KEY_USER_ID).get();
	}

	protected void setKey(Channel ch, String key) {
		ch.attr(AttributteKeys.KEY_USER_ID).set(key);
	}

	protected void checkAuth(Channel ch) throws NotAuthException {
		if (!ch.hasAttr(AttributteKeys.KEY_USER_ID)) {
			throw new NotAuthException();
		}
	}
}
