package com.geek.chat.server;

import java.util.List;

import com.geek.chat.common.codec.ProtobufDecoder;
import com.geek.chat.common.codec.ProtobufEncoder;
import com.geek.chat.common.handler.ChannelHandlerFactory;
import com.geek.chat.common.server.NettyServer;
import com.geek.chat.common.util.PropertiesUtil;
import com.geek.chat.server.handler.ChatServerHandler;

import io.netty.channel.ChannelHandler;

public class ChatServer extends NettyServer{
	//首先会调用父类的构造函数
	private final static NettyServer server = new ChatServer();

	public static NettyServer sharedInstance() {
		return server;
	}


	@Override
	public void setHandlerList(List<ChannelHandlerFactory> handlerList) {
		handlerList.add(new ChannelHandlerFactory() {
			@Override
			public ChannelHandler build() {
				return new ProtobufDecoder();
			}
		});
		handlerList.add(new ChannelHandlerFactory() {

			@Override
			public ChannelHandler build() {
				return new ProtobufEncoder();
			}
		});
		
		handlerList.add(new ChannelHandlerFactory() {

			@Override
			public ChannelHandler build() {
				return new ChatServerHandler();
			}
		});
	}

	@Override
	public int getPort() {
		return PropertiesUtil.getInteger("server.port",5555);
	}


	@Override
	public String getServerIp() {
		return PropertiesUtil.getProperty("server.ip");
	}

	public static void main(String[] args) {
		NettyServer server = ChatServer.sharedInstance();
		server.start();
	}
}
