package com.geek.chat.server.operation.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.geek.chat.common.constant.Constants;
import com.geek.chat.common.protobuf.ProtoMsg.Message;
import com.geek.chat.common.protobuf.builder.ProtobufBuilder;
import com.geek.chat.common.session.Session;
import com.geek.chat.server.operation.AbstractOperation;

public class HeartOperation extends AbstractOperation{
	private Logger logger = LoggerFactory.getLogger(HeartOperation.class);
	@Override
	public Integer op() {
		return Constants.OP_HEARTBEAT;
	}

	@Override
	public void action(Session ch, Message proto) throws Exception {
		logger.info("heartbeat : {}", "clientAddr=" +ch.getRemoteAddress() + " , uid="+ch.getSessionUniqueId());
		//更新活跃时间
		ch.setLastActiveTimeToNow();
		//回应heart reply
		ch.write(ProtobufBuilder.buildHeartRsp(proto.getSeqId()));
	}

}
