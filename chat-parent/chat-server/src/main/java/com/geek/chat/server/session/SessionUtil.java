package com.geek.chat.server.session;

import org.apache.commons.lang.StringUtils;

public class SessionUtil {
	/**
	 * uid+fevId
	 * @param keys
	 * @return
	 */
	public static String getSessionId(String ...keys){
		return StringUtils.join(keys, "-");
	}
}
