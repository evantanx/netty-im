package com.geek.chat.server.operation;

import com.geek.chat.common.protobuf.ProtoMsg;
import com.geek.chat.common.session.Session;

/**
 * 操作类
 */
public interface Operation {

    Integer op();

    void action(Session ch, ProtoMsg.Message proto) throws Exception;

}
