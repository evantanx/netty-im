package com.geek.chat.server.operation;

import java.util.HashMap;
import java.util.Map;

import com.geek.chat.common.util.ClassUtil;

public class OperationContext {

	private static OperationContext instance;
	
	/**Operation坐在包名*/
	private static final String PKG = "com.geek.chat.server.operation.impl";

	public static Map<Integer, Operation> ctx = new HashMap<Integer, Operation>();

	static {
		instance = new OperationContext();
	}
	
	private OperationContext(){
		try{
			initContext();
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	

	public static OperationContext getInstance(){
		return instance;
	}
	
	private void initContext() throws Exception{
		Class<AbstractOperation> cls =AbstractOperation.class;
		for (Class<?> c : ClassUtil.getClasses(cls,PKG)) {
			if (cls.isAssignableFrom(c) && !cls.equals(c)) {
				//获取ITalkAbstractMessage的所有子类消息Class
				Operation operation = (Operation) c.newInstance();
				int op = operation.op();
				ctx.put(op, operation);
			}
		}
	}
	
	public Operation getOperation(int msgId){
		return ctx.get(msgId);
	}
	
	public static void main(String[] args) throws Exception {
		OperationContext context = OperationContext.getInstance();
		context.getOperation(1).action(null, null);
	}

}
