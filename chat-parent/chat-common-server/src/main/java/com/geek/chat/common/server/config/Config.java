package com.geek.chat.common.server.config;

import com.geek.chat.common.util.PropertiesUtil;

public class Config {
	public static class Server {
		public final static int DEFAULT_HEARTBEAT_TIMEOUT_MINISECONDS = 15000 * 5;
		public final static String serverIp = PropertiesUtil.getProperty("server.ip");
		public final static int port = PropertiesUtil.getInteger("server.ip", 5555);
	}
}
