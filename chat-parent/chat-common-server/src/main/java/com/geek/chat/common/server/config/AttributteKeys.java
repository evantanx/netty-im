package com.geek.chat.common.server.config;

import com.geek.chat.common.session.Session;

import io.netty.util.AttributeKey;

public class AttributteKeys {
	public static final AttributeKey<String> KEY_USER_ID = AttributeKey.valueOf("key");
	
	public static final AttributeKey<Session> SESSION = AttributeKey.valueOf("session");
}
