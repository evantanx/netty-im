package com.geek.chat.client.builder;

import org.apache.commons.lang.StringUtils;

import com.geek.chat.common.constant.Constants;
import com.geek.chat.common.protobuf.ProtoMsg.ChatMsg;
import com.geek.chat.common.protobuf.ProtoMsg.LoginInfo;
import com.geek.chat.common.protobuf.ProtoMsg.Message;
import com.geek.chat.common.util.MsgUtil;
/**
 * 消息构建Builder
 * @author lenovo
 *
 */
public class MessageBuilder {
	
	/**
	 * 基类Builder
	 * @author lenovo
	 *
	 */
	public class BaseBuilder {
		protected int head;
		protected long seqId;
		
		public BaseBuilder(){
			seqId = MsgUtil.genSeqId();
		}
		
		public int getHead() {
			return head;
		}
		
		public long getSeqId() {
			return seqId;
		}
		
		/**
		 * 部分构建消息
		 * @return
		 */
		public Message buildPartial(){
			Message.Builder mb = Message.newBuilder()
				   .setHeader(head)
				   .setSeqId(seqId);
			return mb.buildPartial();
		}
	}
	
	/**
	 * 构建心跳消息
	 * @author lenovo
	 *
	 */
	public class HeartBuilder extends BaseBuilder{
		public Message build(){
			Message.Builder mb = Message.newBuilder()
					   .setHeader(head)
					   .setSeqId(seqId);
			return mb.buildPartial();
		}
	}
	
	/**
	 * 登陆授权消息Builder
	 * @author lenovo
	 */
	public class AuthBuilder extends BaseBuilder{
		private String uid;
		private String deviceId;
		private String token;
		private int platform;
		private String appVersion;
		public AuthBuilder(){
			this.head = Constants.OP_AUTH;
		}
		public String getUid() {
			return uid;
		}
		public void setUid(String uid) {
			this.uid = uid;
		}
		public String getDeviceId() {
			return deviceId;
		}
		public void setDeviceId(String deviceId) {
			this.deviceId = deviceId;
		}
		public String getToken() {
			return token;
		}
		public void setToken(String token) {
			this.token = token;
		}
		public int getPlatform() {
			return platform;
		}
		public void setPlatform(int platform) {
			this.platform = platform;
		}
		public String getAppVersion() {
			return appVersion;
		}
		public void setAppVersion(String appVersion) {
			this.appVersion = appVersion;
		}
		
		public Message build(){
			Message message = buildPartial();
			LoginInfo.Builder lb = LoginInfo.newBuilder()
					 .setAppVersion(appVersion)
					 .setDeviceId(deviceId)
					 .setPlatform(platform)
					 .setToken(token)
					 .setUid(uid);
			return message.toBuilder().setLoginInfo(lb).build();
		}
	}

	/**
	 * 聊天消息Builder
	 * @author lenovo
	 *
	 */
	public class ChatMsgBuilder extends BaseBuilder{
		private long msgId;
		private String from;
		private String to;
		private long time;
		private int msgType;         //消息类别   1：纯文本   2：音频  3：视频  4：地理位置   5：其他
		private String content;
		private String url;          //多媒体地址
		private String property;     //附加属性
		private String fromNick;     //发送者昵称
		private int platform;        //1:android, 2:ios, 3:web , 4:both
		private String json;         //附加的json串
		
		public ChatMsgBuilder(){
			this.head = Constants.OP_MESSAGE;
		}
		
		public long getMsgId() {
			return msgId;
		}
		public void setMsgId(long msgId) {
			this.msgId = msgId;
		}
		public String getFrom() {
			return from;
		}
		public void setFrom(String from) {
			this.from = from;
		}
		public String getTo() {
			return to;
		}
		public void setTo(String to) {
			this.to = to;
		}
		public long getTime() {
			return time;
		}
		public void setTime(long time) {
			this.time = time;
		}
		public int getMsgType() {
			return msgType;
		}
		public void setMsgType(int msgType) {
			this.msgType = msgType;
		}
		public String getContent() {
			return content;
		}
		public void setContent(String content) {
			this.content = content;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public String getProperty() {
			return property;
		}
		public void setProperty(String property) {
			this.property = property;
		}
		public String getFromNick() {
			return fromNick;
		}
		public void setFromNick(String fromNick) {
			this.fromNick = fromNick;
		}
		public int getPlatform() {
			return platform;
		}
		public void setPlatform(int platform) {
			this.platform = platform;
		}
		public String getJson() {
			return json;
		}
		public void setJson(String json) {
			this.json = json;
		}
		
		public Message build(){
			Message message = buildPartial();
			ChatMsg.Builder cb = ChatMsg.newBuilder();
			if(msgId >0){
				cb.setMsgId(msgId);
			}
			if(StringUtils.isNotEmpty(from)){
				cb.setFrom(from);
			}
			if(StringUtils.isNotEmpty(to)){
				cb.setTo(to);
			}
			if(time > 0){
				cb.setTime(time);
			}
			if(msgType > 0){
				cb.setMsgType(msgType);
			}
			if(StringUtils.isNotEmpty(content)){
				cb.setContent(content);
			}
			if(StringUtils.isNotEmpty(url)){
				cb.setUrl(url);
			}
			if(StringUtils.isNotEmpty(property)){
				cb.setProperty(property);
			}
			if(StringUtils.isNotEmpty(fromNick)){
				cb.setFromNick(fromNick);
			}
			if(platform >0){
				cb.setPlatform(platform);
			}
			if(StringUtils.isNotEmpty(json)){
				cb.setJson(json);
			}
			return message.toBuilder().setChatMsg(cb).build();
		}
	}
}
