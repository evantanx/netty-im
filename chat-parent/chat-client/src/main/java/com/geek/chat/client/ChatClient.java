package com.geek.chat.client;

import java.io.InputStreamReader;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.geek.chat.client.builder.MessageBuilder;
import com.geek.chat.client.handler.ChatClientHandler;
import com.geek.chat.common.client.NettyClient;
import com.geek.chat.common.codec.ProtobufDecoder;
import com.geek.chat.common.codec.ProtobufEncoder;
import com.geek.chat.common.constant.Platform;
import com.geek.chat.common.handler.ChannelHandlerFactory;
import com.geek.chat.common.protobuf.ProtoMsg.Message;
import com.geek.chat.common.protobuf.builder.ProtobufBuilder;
import com.geek.chat.common.util.MsgUtil;
import com.geek.chat.common.util.PropertiesUtil;

import io.netty.channel.ChannelHandler;

public class ChatClient extends NettyClient {
	private static Logger logger = LoggerFactory.getLogger(ChatClient.class);

	@Override
	public void setHandlerList(List<ChannelHandlerFactory> handlerList) {
		/**
		 * 添加消息解码器
		 */
		handlerList.add(new ChannelHandlerFactory() {

			@Override
			public ChannelHandler build() {
				return new ProtobufDecoder();
			}

		});

		/**
		 * 添加消息编码器
		 */
		handlerList.add(new ChannelHandlerFactory() {

			@Override
			public ChannelHandler build() {
				return new ProtobufEncoder();
			}

		});

		/**
		 * 添加核心消息处理Handler
		 */
		handlerList.add(new ChannelHandlerFactory() {

			@Override
			public ChannelHandler build() {
				return new ChatClientHandler();
			}

		});
	}

	@Override
	public int getPort() {
		return PropertiesUtil.getInteger("server.port", 5555);
	}

	@Override
	public String getServerIp() {
		return PropertiesUtil.getProperty("server.ip");
	}

	@Override
	protected Message getHeartbeatDataPackage() {
		return ProtobufBuilder.buildHeartReq(MsgUtil.genSeqId());
	}

	@Override
	protected int autoReconnectTimesThreshold() {
		return Integer.MAX_VALUE;
	}

	public static void main(String[] args) {
		/**
		ExecutorService pool = Executors.newCachedThreadPool();
		for (int i = 0; i < 100; i++) {
			pool.execute(new Runnable() {
				@Override
				public void run() {

					final ChatClient client = new ChatClient();
					new Thread(new Runnable() {
						@Override
						public void run() {

							try {
								Thread.sleep(3000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}

							Scanner sc = new Scanner(new InputStreamReader(System.in));
							System.out.println("请输入命令...");
							boolean flag = true;
							while (flag) {
								int op = sc.nextInt();
								if (op == 1) {
									// 登陆请求
									sendAuthMsg(client);
								} else if (op == 2) {
									sendChatMsg(client);
								} else if (op == -1) {
									sc.close();
									System.exit(0);
								}
							}
						}
					}).start();
					client.start();
				}
			});
		}
		*/
		
		final ChatClient client = new ChatClient();
		ExecutorService pool = Executors.newCachedThreadPool();
		pool.execute(new Runnable(){

			@Override
			public void run() {
				Scanner sc = new Scanner(new InputStreamReader(System.in));
				System.out.println("请输入命令...");
				boolean flag = true;
				while (flag) {
					int op = sc.nextInt();
					if (op == 1) {
						// 登陆请求
						sendAuthMsg(client);
					} else if (op == 2) {
						sendChatMsg(client);
					} else if (op == -1) {
						sc.close();
						System.exit(0);
					}
				}
			}
			
		});
	}

	private static void sendAuthMsg(ChatClient client) {
		logger.info("开始登陆");
		MessageBuilder.AuthBuilder builder = new MessageBuilder().new AuthBuilder();
		builder.setAppVersion("1.0");
		builder.setDeviceId("8ca9995d212f4d4b875905580e8c72a7");// 设备ID
		builder.setPlatform(Platform.ANDROID); // OS平台类型
		builder.setToken("767676YUYUG767678HGJGg"); // 登陆的token信息
		builder.setUid("8745d21c549b11e69dc71051721cd16d"); // 用户子系统中的唯一标示
		client.write(builder.build());
		logger.info("结束登陆");
	}

	private static void sendChatMsg(ChatClient client) {
		logger.info("开始发送消息");
		MessageBuilder.ChatMsgBuilder builder = new MessageBuilder().new ChatMsgBuilder();
		builder.setContent("测试发送消息");
		builder.setFrom("8745d21c549b11e69dc71051721cd16d");
		builder.setTo("123456");
		builder.setFromNick("小小");
		builder.setPlatform(Platform.ANDROID);
		builder.setMsgType(1);
		builder.setTime(System.currentTimeMillis());
		builder.setMsgId(System.currentTimeMillis());
		client.write(builder.build());
		logger.info("发送消息结束");
	}

}
